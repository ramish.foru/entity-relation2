package com.example.entityrelation2.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
@Table(name = "product_data")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Integer id;
    private String productName;
    private String productCode;
    private Double price;

}
