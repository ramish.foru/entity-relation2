package com.example.entityrelation2.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class ProductDto {

    private Integer id;

    @NotBlank (message = "Product name cannot be blank")
    private String productName;
    private String productCode;
    private Double price;

}
