package com.example.entityrelation2.controller;

import com.example.entityrelation2.dto.CostumerDto;
import com.example.entityrelation2.entity.Costumer;
import com.example.entityrelation2.service.CostumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/costumer")
public class CostumerController {

    @Autowired
    private CostumerService costumerService;

    @PostMapping("/save")
    public ResponseEntity<?> createCostumerData(@Valid @RequestBody CostumerDto costumerDto){
        return new ResponseEntity<>(costumerService.createCostumerData(costumerDto), HttpStatus.OK);
    }

    @PutMapping("/update")
    public Costumer updateCostumerData(@RequestBody CostumerDto costumerDto){
        return costumerService.updateCostumerData(costumerDto);
    }

    @GetMapping("/get/all")
    public List<Costumer> getAllData(){
        return costumerService.getAllData();
    }

    @GetMapping("/id/{id}")
    public Costumer getDataById(@PathVariable Integer id){
        return costumerService.getDataById(id);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id){
        costumerService.delete(id);
    }
}

