package com.example.entityrelation2.controller;

import com.example.entityrelation2.dto.ProductDto;
import com.example.entityrelation2.entity.Product;
import com.example.entityrelation2.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/save")
    public ResponseEntity<?> createProductData(@Valid @RequestBody ProductDto productDto){
        return new ResponseEntity<>(productService.createProductData(productDto), HttpStatus.OK);
    }

    @PutMapping("/update")
    public Product updateProductData(@RequestBody ProductDto productDto){
        return productService.updateProductData(productDto);
    }

    @GetMapping("/get/all")
    public List<Product> getAllData(){
        return productService.getAllData();
    }

    @GetMapping("/id/{id}")
    public Product getDataById(@PathVariable Integer id){
        return productService.getDataById(id);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id){
        productService.delete(id);
    }
}

