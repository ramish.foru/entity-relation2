package com.example.entityrelation2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntityRelation2Application {

    public static void main(String[] args) {
        SpringApplication.run(EntityRelation2Application.class, args);
    }

}
