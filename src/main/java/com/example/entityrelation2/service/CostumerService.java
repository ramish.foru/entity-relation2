package com.example.entityrelation2.service;

import com.example.entityrelation2.dto.CostumerDto;
import com.example.entityrelation2.entity.Costumer;

import java.util.List;

public interface CostumerService {

    Costumer createCostumerData(CostumerDto costumerDto);

    Costumer updateCostumerData(CostumerDto costumerDto);

    List<Costumer> getAllData();

    Costumer getDataById(Integer id);

    void delete(Integer id);
}
