package com.example.entityrelation2.service;

import com.example.entityrelation2.dto.CostumerDto;
import com.example.entityrelation2.entity.Costumer;
import com.example.entityrelation2.entity.Product;
import com.example.entityrelation2.repository.CostumerRepository;
import com.example.entityrelation2.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CostumerServiceImpl implements CostumerService {

    @Autowired
    private CostumerRepository costumerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Costumer createCostumerData(CostumerDto costumerDto) {

       Product product =productRepository.findById(costumerDto.getProductId()).orElse(null);

        return costumerRepository.save(Costumer.builder()

                        .name(costumerDto.getName())
                        .address(costumerDto.getAddress())
                        .mobileNumber(costumerDto.getMobileNumber())

                        .product(product)

                .build());
    }

    @Override
    public Costumer updateCostumerData(CostumerDto costumerDto) {
        Product product = productRepository.findById(costumerDto.getProductId()).orElse(null);

        Costumer costumer = costumerRepository.findById(costumerDto.getId()).orElse(null);

        return costumerRepository.save(Costumer.builder()

                        .id(costumer.getId())

                        .name(costumerDto.getName())
                        .mobileNumber(costumerDto.getMobileNumber())
                        .address(costumerDto.getAddress())

                        .product(product)

                .build());
    }

    @Override
    public List<Costumer> getAllData() {
        return costumerRepository.findAll();
    }

    @Override
    public Costumer getDataById(Integer id) {
        return costumerRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(Integer id) {
        costumerRepository.deleteById(id);

    }
}
