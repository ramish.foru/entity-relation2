package com.example.entityrelation2.service;

import com.example.entityrelation2.dto.ProductDto;
import com.example.entityrelation2.entity.Product;

import java.util.List;

public interface ProductService {

    Product createProductData(ProductDto productDto);

    Product updateProductData(ProductDto productDto);

    List<Product> getAllData();

    Product getDataById(Integer id);

    void delete(Integer id);

}
