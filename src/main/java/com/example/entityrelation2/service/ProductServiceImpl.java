package com.example.entityrelation2.service;

import com.example.entityrelation2.dto.ProductDto;
import com.example.entityrelation2.entity.Product;
import com.example.entityrelation2.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired

    private ProductRepository productRepository;


    @Override
    public Product createProductData(ProductDto productDto) {
        Product product = productRepository.save(Product.builder()
                        .productName(productDto.getProductName())
                        .productCode(productDto.getProductCode())
                        .price(productDto.getPrice())
                .build());

        return product;
    }

    @Override
    public Product updateProductData(ProductDto productDto) {

        Product product = productRepository.findById(productDto.getId()).orElse(null);

        return productRepository.save(Product.builder()

                        .id(product.getId())

                        .productName(productDto.getProductName())
                        .productCode(productDto.getProductCode())
                        .price(productDto.getPrice())

                .build());
    }

    @Override
    public List<Product> getAllData() {
        return productRepository.findAll();
    }

    @Override
    public Product getDataById(Integer id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(Integer id) {
        productRepository.deleteById(id);

    }
}
